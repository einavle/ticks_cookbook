# frozen_string_literal: true
# This is a Chef recipe file. It can be used to specify resources which will
# apply configuration to a server.

# For more information, see the documentation: https://docs.chef.io/recipes.html


yum_package "influxdb" do
     action :install
     version "#{node['ticks']['influxdb']['version']}"
   end

yum_package "telegraf" do
     action :install
     version "#{node['ticks']['telegraf']['version']}"
   end

yum_package "chronograf" do
     action :install
     version "#{node['ticks']['chronograf']['version']}"
   end
service 'influxdb' do
  supports :status => true, :restart => true, :reload => true
  action   [:start, :enable]
end

service 'telegraf' do
  supports :status => true, :restart => true, :reload => true
  action   [:start, :enable]
end

service 'chronograf' do
  supports :status => true, :restart => true, :reload => true
  action   [:start, :enable]
end