# frozen_string_literal: true
# This is a Chef attributes file. It can be used to specify default and override
# attributes to be applied to nodes that run this cookbook.

# Set a default name
default['ticks']['influxdb']['version'] = '1.6.4-1'
default['ticks']['chronograf']['version'] = '1.7.1-1'
default['ticks']['telegraf']['version'] = '1.8.3-1'


# For further information, see the Chef documentation (https://docs.chef.io/attributes.html).
